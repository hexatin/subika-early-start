es = '../es.csv'
optimal = '../solution-no-early-starts.out'

optimal_es = {}
algo_es = {}

with open(es, 'r') as infile:
    buf = infile.read()
    buf = buf.split('\n')
    for idx,line in enumerate(buf):
        buf[idx] = line.split(',')
    for line in buf:
        if(len(line) > 1):
            algo_es[line[0]] = int(line[1])
    
with open(optimal, 'r') as infile:
    buf = infile.read()
    buf = buf.split('\n')
    buf = buf[3:]
    for idx,line in enumerate(buf):
        buf[idx] = line.split()
    for line in buf:
        if(len(line) > 1):
            optimal_es[line[0]] = int(line[1])

for act in optimal_es:
    if(algo_es[act] > optimal_es[act]):
        print('ERROR AT ' + act)
        print(algo_es[act])
        print(optimal_es[act])
