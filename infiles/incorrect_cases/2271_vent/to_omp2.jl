using Dates

function readInstFile(argInstName)
    fProb=open(argInstName*".prb")
    fAct=open(argInstName*".act")
    fStl=open(argInstName*".stl")
    fRes=open(argInstName*".res")
    fPre=open(argInstName*".pre")
    fDel=open(argInstName*".del")
    fLvl=open(argInstName*".lvl")#changed for q_[a,n]
    ### .prob file ###
    line=readline(fProb)
    tMax=parse(Int,line)
    line=readline(fProb)
    dRate=parse(Float64,line)
#    ###################
#    #### .res file ###
    line=readline(fRes)
    nRes=parse(Int,line)
    resAvl=zeros(Float64,nRes,tMax)
    for r=1:nRes
        line=split(readline(fRes))
        for t=1:tMax
            resAvl[r,t]=parse(Float64,line[t])
        end
    end
#    ##################
#    #### .act file ###
    line=readline(fAct)
    nActs=parse(Int,line)
    dur=zeros(Int,nActs)
    profit=zeros(Float64,nActs)
    resCon=prof=zeros(Float64,nActs,nRes)
    for a=1:nActs
        line=split(readline(fAct))
        dur[a]=parse(Int,line[2])
        profit[a]=parse(Float64,line[3])
        for r=1:nRes
            resCon[a,r]=parse(Float64,line[3+r])
        end
    end
#    ##################
#    #### .stl file ###
    line=readline(fStl)
    nStages=parse(Int,line)
    line=readline(fStl)
    fCost=-parse(Float64,line)
    line=readline(fStl)
    nLevels=parse(Int,line)
    hCap=zeros(Float64,nLevels)
    line=split(readline(fStl))
    for n=1:nLevels
        hCap[n]=parse(Float64,line[n])
    end
    gStages=zeros(Float64,nStages,nLevels)
    for s=1:nStages
        line=split(readline(fStl))
        for n=1:nLevels
            gStages[s,n]=parse(Float64,line[n])#negative value now
        end
    end
#    ##################
#    #### .pre file ###
    pred=Array{Array{Int}}(undef,nActs)##IMPORTANT!!!activities start from 0 here
    for a=1:nActs
        line=split(readline(fPre))
        nPreds=parse(Int,line[2])
        pred[a]=[parse(Int,line[2+i]) for i=1:nPreds]
    end
#    ##################
#    #### .del file ###
    delays=Array{Array{Int}}(undef,nActs)
    for a=1:nActs
        line=split(readline(fDel))
        nPreds=parse(Int,line[2])
        delays[a]=[parse(Int,line[2+i]) for i=1:nPreds]
    end
	 ##################
	 #### .lvl file ###
	qActs=zeros(Float64,nActs,nLevels)
	for a=1:nActs
		line=split(readline(fLvl))
		for n=1:nLevels
			qActs[a,n]=parse(Float64,line[n])
		end
	end
#    ##################
    close(fProb)
    close(fAct)
    close(fStl)
    close(fRes)
    close(fPre)
    close(fDel)
    return(tMax,nActs,nStages,nLevels,nRes,dRate,fCost,dur,profit,resCon,qActs,hCap,gStages,resAvl,pred,delays)
end

function write_to_omp(fname,tMax,nActs,nStages,nLevels,nRes,dRate,fCost,dur,profit,resCon,qActs,hCap,gStages,resAvl,pred,delays)
    prob_f=open("omp_"*fname*".prob","w")
    blocks_f=open("omp_"*fname*".blocks","w")
    delay_f=open("omp_"*fname*".delay","w")
    params_f=open("omp_"*fname*".params","w")
    prec_f=open("omp_"*fname*".prec","w")
    ########################## .params file #########################################
    write(params_f,"USE_DISPLAY: 1\n")
	write(params_f,"ANALYSIS: 24\n")
	write(params_f,"INTEGRALITY_VIOLATION_LIMIT: 1e-2\n")
	write(params_f,"PRECEDENCE_VIOLATION_LIMIT: 1e-5\n")
	write(params_f,"CONSTRAINT_VIOLATION_LIMIT: 5e-5\n")#chaged
	write(params_f,"BOUND_VIOLATION_LIMIT: 1e-5\n")
	write(params_f,"WRITE.LP.SOLUTION: 1\n")
	write(params_f,"WRITE.IP.SOLUTION: 1\n")
	write(params_f,"PP.ULTIMATE_PIT: 0\n")#changed
	write(params_f,"PP.FORCE_UPIT: 0\n")#changed
	write(params_f,"PP.ELIM_NULL: 1\n")
	write(params_f,"PP.EARLY_START: 1\n")
	write(params_f,"PP.WASTE_OPTION: 0\n")
	write(params_f,"PP.TRANSITIVE_REDUCTION: 1\n")
	write(params_f,"OPTMETHOD: 0\n")
	write(params_f,"CG.ONE_DESTINATION: 1\n")
	write(params_f,"CG.IMPLICIT: 0\n")
	write(params_f,"CG.USE_DISPLAY: 1\n")
	write(params_f,"CG.MAX_ITER: -1\n")
	write(params_f,"CG.TARGET_GAP: 0.01\n")
	write(params_f,"CG.MAX_TIME: -1\n")
	write(params_f,"CG.USE_PHS: 1\n")
	write(params_f,"CG.USE_MHS: 1\n")
	write(params_f,"CG.BASIS_DUMP: 1\n")
	write(params_f,"CG.USE_KSTEP: 1\n")
	write(params_f,"CG.KSTEP_K: 10\n")
	write(params_f,"IP.MAX_SOLS: 0\n")
	write(params_f,"IP.TARGET_GAP: 1e-5\n")
	write(params_f,"HE.TLIM: 0\n")
	write(params_f,"HE.TOPOSORT: 1\n")
	write(params_f,"HE.FTOPOSORT: 0\n")
	write(params_f,"HE.NALPHA_POINTS: 50\n")
	write(params_f,"HE.OPT_DESTINATIONS: 1\n")
	write(params_f,"HE.UPIT_POSTPROCESS: 0\n")
	write(params_f,"DBG.ROWSCALE: 0\n")
    #################################################################################
    ########################## .prob file ###########################################
    write(prob_f,"NDESTINATIONS: 1\n")
    write(prob_f,"NPERIODS: $(tMax)\n")
    write(prob_f,"DISCOUNT_RATE: $(dRate)\n")
    write(prob_f,"OBJECTIVE: 0 2\n")
    write(prob_f,"DURATION: 1\n")
    write(prob_f,"NCONSTRAINTS: $(nRes+nLevels)\n")
    constraint=0
    for r=1:nRes
        write(prob_f,"CONSTRAINT: $(constraint) $(3+constraint) P 0 L")
        for t=1:tMax
            write(prob_f," $(resAvl[r,t])")
        end
        write(prob_f,"\n")
        constraint+=1
    end
    for n=1:nLevels
        write(prob_f,"CONSTRAINT: $(constraint) $(3+constraint) P 0 L $(hCap[n])\n")
        write(prob_f,"\n")
        constraint+=1
    end
    #################################################################################
    ########################## .blocks file #########################################
    cor=0
    for a=1:nActs
        write(blocks_f,"$(cor) $(dur[a]) $(profit[a])")
        for r=1:nRes
            write(blocks_f," $(resCon[a,r])")
        end
        for n=1:nLevels
            write(blocks_f," $(qActs[a,n])")
        end
        write(blocks_f,"\n")
        cor=cor+1
    end
    for s=1:nStages
        write(blocks_f,"$(cor) $(tMax) $(fCost)")
        for r=1:nRes
            write(blocks_f," 0")
        end
        for n=1:nLevels
            write(blocks_f," $(gStages[s,n])")
        end
        write(blocks_f,"\n")
        cor=cor+1
    end
#    #################################################################################
#    ########################## .prec file ###########################################
    cor=0
    for a=1:nActs
        write(prec_f,"$(cor) $(size(pred[a])[1])")
        for p in pred[a]
            write(prec_f," $p")
        end
        cor+=1
        write(prec_f,"\n")
    end
    write(prec_f,"$(cor) 0\n")
    cor+=1
    for s=2:nStages
        write(prec_f,"$(cor) 1 $(cor-1)\n")
        cor+=1
    end
#    #################################################################################
#    ########################## .delay file ###########################################
    cor=0
    for a=1:nActs
        write(delay_f,"$(cor) $(size(pred[a])[1])")
        for d in delays[a]
            write(delay_f," $(dur[a]+d)")
        end
        cor+=1
        write(delay_f,"\n")
    end
    write(delay_f,"$(cor) 0\n")
    cor+=1
    for s=2:nStages
        write(delay_f,"$(cor) 1 0\n")
        cor+=1
    end
    #################################################################################
    close(prob_f)
    close(blocks_f)
    close(delay_f)
    close(params_f)
    close(prec_f)
end

function read_lp_solution(tMax,nActs,nStages,fname)
    sol_f=open(fname,"r")
    lp_sol_y=zeros(Float64,nActs,tMax)
    lp_sol_z=zeros(Float64,nStages,tMax)
    while true
        line=split(readline(sol_f))
        if length(line)<=1
            break
        end
        a=parse(Int,line[1])+1
        t=parse(Int,line[3])+1
        sol=parse(Float64,line[4])
        if a<=nActs
            lp_sol_y[a,t]=sol
        else
            lp_sol_z[a%nActs,t]=sol
        end
    end
    close(sol_f)
    return (lp_sol_y,lp_sol_z)
end

function create_list(lp_sol_y,lp_sol_z,tMax,nActs,nStages)
    p_list_y=[sum(t*lp_sol_y[a,t] for t=1:tMax) for a=1:nActs]
    p_list_z=[sum(t*lp_sol_z[s,t] for t=1:tMax) for s=1:nStages]
    return(p_list_y,p_list_z)
end

function check_pred_feas(a,t,started,start_times,finish_times,pred,delays)
    if all(x->(x+1 in started),pred[a])
        for i=1:length(pred[a])
            if finish_times[findfirst(isequal(pred[a][i]+1),started)]+delays[a][i]>=t
                return false
            end
        end
        return true
#        if all(x->(finish_times[findfirst(isequal(x+1),started)]<t),pred[a])
#            return true
#        end
    end    
    return false
end

function check_res_feas(a,t,res_profile,nRes,resCon,tMax,dur)
    for r=1:nRes,tp=t:min(tMax,t+dur[a]-1)
        if res_profile[r,tp]-resCon[a,r]<0
            return false
        end
    end
    return true
end

function check_heat_feas(a,t,heat_profile,nLevels,qActs,tMax,dur)
    for n=1:nLevels,tp=t:min(tMax,t+dur[a]-1)
        if heat_profile[n,tp]-qActs[a,n]<0
            return false
        end
    end
    return true
end

function toposort(tMax,nActs,p_list_y,nLevels,nRes,dur,profit,resCon,qActs,heat_profile,resAvl,pred,delays)
    not_started=[a for a=1:nActs if p_list_y[a]>0]
#    println(length(not_started))
    sort!(not_started,by=a->(p_list_y[a]))
    started=[]
    start_times=[]
    finish_times=[]
    res_profile=copy(resAvl)
    heat_profilep=copy(heat_profile)
    t=1
    while length(not_started)>0 && t<=tMax
        to_delete=[]
        for i=1:length(not_started)
            if check_pred_feas(not_started[i],t,started,start_times,finish_times,pred,delays) && check_res_feas(not_started[i],t,res_profile,nRes,resCon,tMax,dur) && check_heat_feas(not_started[i],t,heat_profilep,nLevels,qActs,tMax,dur)
                push!(to_delete,not_started[i])
                push!(started,not_started[i])
                push!(start_times,t)
                push!(finish_times,t+dur[not_started[i]]-1)
		        for tp=t:min(tMax,t+dur[not_started[i]]-1)
		            for r=1:nRes
		                res_profile[r,tp]=res_profile[r,tp]-resCon[not_started[i],r]
		            end
		            for n=1:nLevels
                        heat_profilep[n,tp]=heat_profilep[n,tp]-qActs[not_started[i],n]
                    end 
                end
            end
        end
        not_started=setdiff(not_started,to_delete)
        t+=1
    end
    return(started,start_times)
end

function calc_heat_profile!(heat_profile,refr_starts,nStages,gStages,nLevels,tMax)
    for s=1:nStages
        if 1<=refr_starts[s]<=tMax
            for n=1:nLevels,t=refr_starts[s]:tMax
                heat_profile[n,t]-=gStages[s,n]
            end
        end
    end
end

function local_search(tMax,nActs,p_list_y,p_list_z,nStages,gStages,nLevels,nRes,dur,profit,resCon,qActs,hCap,resAvl,pred,delays,fCost,dRate)
    heat_profile=zeros(Float64,nLevels,tMax)
    for n=1:nLevels,t=1:tMax
        heat_profile[n,t]=hCap[n]
    end
    ############################# without refrigeration #############################
#    (started,start_times)=toposort(tMax,nActs,p_list_y,nLevels,nRes,dur,profit,resCon,qActs,heat_profile,resAvl,pred,delays)
#    println(length(started))
#    obj_value=sum((1+dRate)^(-start_times[i]+1)*profit[started[i]] for i=1:length(started))
#    println(obj_value)
    ################################ with refrigeration #############################
    exec_time=now()
    refr_starts=[Int(ceil(p_list_z[s])) for s=1:nStages]
    calc_heat_profile!(heat_profile,refr_starts,nStages,gStages,nLevels,tMax)
    (started,start_times)=toposort(tMax,nActs,p_list_y,nLevels,nRes,dur,profit,resCon,qActs,heat_profile,resAvl,pred,delays)
    exec_time=now()-exec_time
    println("Execution Time: $(exec_time)")
    println("Executed Activities: $(length(started))")
    obj_value=sum((1+dRate)^(-start_times[i]+1)*profit[started[i]] for i=1:length(started))+sum((1+dRate)^(-refr_starts[s]+1)*fCost for s=1:nStages if refr_starts[s]>0)
    println("Objective Value: $(obj_value)")
    for s=1:nStages
        println("Refigeration stage $(s) starts at period $(refr_starts[s]-1)")
    end
    for i=1:length(started)
        println("$(started[i]-1) $(start_times[i]-1)")
    end
end

###########################################################################################

argInstName=ARGS[1]
lp_sol_file="omp_2271_vent_omp_2271_vent_omp_2271_vent.BZ.lp.sol"

(tMax,nActs,nStages,nLevels,nRes,dRate,fCost,dur,profit,resCon,qActs,hCap,gStages,resAvl,pred,delays)=readInstFile(argInstName)
(lp_sol_y,lp_sol_z)=read_lp_solution(tMax,nActs,nStages,lp_sol_file)
(p_list_y,p_list_z)=create_list(lp_sol_y,lp_sol_z,tMax,nActs,nStages)
local_search(tMax,nActs,p_list_y,p_list_z,nStages,gStages,nLevels,nRes,dur,profit,resCon,qActs,hCap,resAvl,pred,delays,fCost,dRate)
#write_to_omp(argInstName,tMax,nActs,nStages,nLevels,nRes,dRate,fCost,dur,profit,resCon,qActs,hCap,gStages,resAvl,pred,delays)
