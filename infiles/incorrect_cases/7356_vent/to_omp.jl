function readInstFile(argInstName)
    fProb=open(argInstName*".prb")
    fAct=open(argInstName*".act")
    fStl=open(argInstName*".stl")
    fRes=open(argInstName*".res")
    fPre=open(argInstName*".pre")
    fDel=open(argInstName*".del")
	fLvl=open(argInstName*".lvl")
    ### .prob file ###
    line=readline(fProb)
    tMax=parse(Int,line)
    line=readline(fProb)
    dRate=parse(Float64,line)
#    ###################
#    #### .res file ###
    line=readline(fRes)
    nRes=parse(Int,line)
    resAvl=zeros(Float64,nRes,tMax)
    for r=1:nRes
        line=split(readline(fRes))
        for t=1:tMax
            resAvl[r,t]=parse(Float64,line[t])
        end
    end
#    ##################
#    #### .act file ###
    line=readline(fAct)
    nActs=parse(Int,line)
    dur=zeros(Int,nActs)
    profit=zeros(Float64,nActs)
    resCon=prof=zeros(Float64,nActs,nRes)
    for a=1:nActs
        line=split(readline(fAct))
        dur[a]=parse(Int,line[2])
        profit[a]=parse(Float64,line[3])
        for r=1:nRes
            resCon[a,r]=parse(Float64,line[3+r])
        end
    end
#    ##################
#    #### .stl file ###
    line=readline(fStl)
    nStages=parse(Int,line)
    line=readline(fStl)
    fCost=-parse(Float64,line)
    line=readline(fStl)
    nLevels=parse(Int,line)
    hCap=zeros(Float64,nLevels)
    line=split(readline(fStl))
    for n=1:nLevels
        hCap[n]=parse(Float64,line[n])
    end
    gStages=zeros(Float64,nStages,nLevels)
    for s=1:nStages
        line=split(readline(fStl))
        for n=1:nLevels
            gStages[s,n]=parse(Float64,line[n])
        end
    end
#    ##################
#    #### .pre file ###
    pred=Array{Array{Int}}(undef,nActs)
    for a=1:nActs
        line=split(readline(fPre))
        nPreds=parse(Int,line[2])
        pred[a]=[parse(Int,line[2+i]) for i=1:nPreds]
    end
#    ##################
#    #### .del file ###
    delays=Array{Array{Int}}(undef,nActs)
    for a=1:nActs
        line=split(readline(fDel))
        nPreds=parse(Int,line[2])
        delays[a]=[parse(Int,line[2+i]) for i=1:nPreds]
    end
	 ##################
	 #### .lvl file ###
	qActs=zeros(Float64,nActs,nLevels)
	for a=1:nActs
		line=split(readline(fLvl))
		for n=1:nLevels
			qActs[a,n]=parse(Float64,line[n])
		end
	end
#    ##################
    close(fProb)
    close(fAct)
    close(fStl)
    close(fRes)
    close(fPre)
    close(fDel)
    return(tMax,nActs,nStages,nLevels,nRes,dRate,fCost,dur,profit,resCon,qActs,hCap,gStages,resAvl,pred,delays)
end

function write_to_omp(fname,tMax,nActs,nStages,nLevels,nRes,dRate,fCost,dur,profit,resCon,qActs,hCap,gStages,resAvl,pred,delays)
    prob_f=open("omp_"*fname*".prob","w")
    blocks_f=open("omp_"*fname*".blocks","w")
    delay_f=open("omp_"*fname*".delay","w")
    params_f=open("omp_"*fname*".params","w")
    prec_f=open("omp_"*fname*".prec","w")
    ########################## .params file #########################################
    write(params_f,"USE_DISPLAY: 1\n")
	write(params_f,"ANALYSIS: 24\n")
	write(params_f,"INTEGRALITY_VIOLATION_LIMIT: 1e-2\n")
	write(params_f,"PRECEDENCE_VIOLATION_LIMIT: 1e-5\n")
	write(params_f,"CONSTRAINT_VIOLATION_LIMIT: 5e-5\n")
	write(params_f,"BOUND_VIOLATION_LIMIT: 1e-5\n")
	write(params_f,"WRITE.LP.SOLUTION: 1\n")
	write(params_f,"WRITE.IP.SOLUTION: 1\n")
	write(params_f,"PP.ULTIMATE_PIT: 0\n")
	write(params_f,"PP.FORCE_UPIT: 0\n")
	write(params_f,"PP.ELIM_NULL: 1\n")
	write(params_f,"PP.EARLY_START: 1\n")
	write(params_f,"PP.WASTE_OPTION: 0\n")
	write(params_f,"PP.TRANSITIVE_REDUCTION: 1\n")
	write(params_f,"OPTMETHOD: 0\n")
	write(params_f,"CG.ONE_DESTINATION: 1\n")
	write(params_f,"CG.IMPLICIT: 0\n")
	write(params_f,"CG.USE_DISPLAY: 1\n")
	write(params_f,"CG.MAX_ITER: -1\n")
	write(params_f,"CG.TARGET_GAP: 0.01\n")
	write(params_f,"CG.MAX_TIME: -1\n")
	write(params_f,"CG.USE_PHS: 1\n")
	write(params_f,"CG.USE_MHS: 1\n")
	write(params_f,"CG.BASIS_DUMP: 1\n")
	write(params_f,"CG.USE_KSTEP: 1\n")
	write(params_f,"CG.KSTEP_K: 10\n")
	write(params_f,"IP.MAX_SOLS: 0\n")
	write(params_f,"IP.TARGET_GAP: 1e-5\n")
	write(params_f,"HE.TLIM: 0\n")
	write(params_f,"HE.TOPOSORT: 1\n")
	write(params_f,"HE.FTOPOSORT: 0\n")
	write(params_f,"HE.NALPHA_POINTS: 50\n")
	write(params_f,"HE.OPT_DESTINATIONS: 1\n")
	write(params_f,"HE.UPIT_POSTPROCESS: 0\n")
	write(params_f,"DBG.ROWSCALE: 0\n")
    #################################################################################
    ########################## .prob file ###########################################
    write(prob_f,"NDESTINATIONS: 1\n")
    write(prob_f,"NPERIODS: $(tMax)\n")
    write(prob_f,"DISCOUNT_RATE: $(dRate)\n")
    write(prob_f,"OBJECTIVE: 0 2\n")
    write(prob_f,"DURATION: 1\n")
    write(prob_f,"NCONSTRAINTS: $(nRes+nLevels)\n")
    constraint=0
    for r=1:nRes
        write(prob_f,"CONSTRAINT: $(constraint) $(3+constraint) P 0 L")
        for t=1:tMax
            write(prob_f," $(resAvl[r,t])")
        end
        write(prob_f,"\n")
        constraint+=1
    end
    for n=1:nLevels
        write(prob_f,"CONSTRAINT: $(constraint) $(3+constraint) P 0 L $(hCap[n])\n")
        write(prob_f,"\n")
        constraint+=1
    end
    #################################################################################
    ########################## .blocks file #########################################
    cor=0
    for a=1:nActs
        write(blocks_f,"$(cor) $(dur[a]) $(profit[a])")
        for r=1:nRes
            write(blocks_f," $(resCon[a,r])")
        end
        for n=1:nLevels
            write(blocks_f," $(qActs[a,n])")
        end
        write(blocks_f,"\n")
        cor=cor+1
    end
    for s=1:nStages
        write(blocks_f,"$(cor) $(tMax) $(fCost)")
        for r=1:nRes
            write(blocks_f," 0")
        end
        for n=1:nLevels
            write(blocks_f," $(gStages[s,n])")
        end
        write(blocks_f,"\n")
        cor=cor+1
    end
#    #################################################################################
#    ########################## .prec file ###########################################
    cor=0
    for a=1:nActs
        write(prec_f,"$(cor) $(size(pred[a])[1])")
        for p in pred[a]
            write(prec_f," $p")
        end
        cor+=1
        write(prec_f,"\n")
    end
    write(prec_f,"$(cor) 0\n")
    cor+=1
    for s=2:nStages
        write(prec_f,"$(cor) 1 $(cor-1)\n")
        cor+=1
    end
#    #################################################################################
#    ########################## .delay file ###########################################
    cor=0
    for a=1:nActs
        write(delay_f,"$(cor) $(size(pred[a])[1])")
        for d in delays[a]
            write(delay_f," $(dur[a]+d)")
        end
        cor+=1
        write(delay_f,"\n")
    end
    write(delay_f,"$(cor) 0\n")
    cor+=1
    for s=2:nStages
        write(delay_f,"$(cor) 1 0\n")
        cor+=1
    end
    #################################################################################
    close(prob_f)
    close(blocks_f)
    close(delay_f)
#    close(params_f)
    close(prec_f)
end

###########################################################################################

argInstName=ARGS[1]

(tMax,nActs,nStages,nLevels,nRes,dRate,fCost,dur,profit,resCon,qActs,hCap,gStages,resAvl,pred,delays)=readInstFile(argInstName)
write_to_omp(argInstName,tMax,nActs,nStages,nLevels,nRes,dRate,fCost,dur,profit,resCon,qActs,hCap,gStages,resAvl,pred,delays)

