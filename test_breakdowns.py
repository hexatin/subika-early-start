import itertools as it

def breakdown(x):
    bds = set()
    if( len(x) <= 1):
        return {frozenset({frozenset(x)})}
    for l in range(1, len(x)+1):
        comb = set(it.combinations(x,l))
        #print(f'Combos of {x} of length {l}: {comb}')
        for c in comb:
            cprime = x - set(c)
            if(len(cprime) == 0):
                bds.add(frozenset({frozenset(c)}))
                continue
            cprime_bds = breakdown(cprime)
            for bd in cprime_bds:
                bds.add(frozenset({frozenset(c)} | bd))
    return bds

def maketup(fs):
    if(type(fs) != frozenset and type(fs) != set):
        return fs
    else:
        return ([maketup(x) for x in fs])

#acts = {1, 2, 3}
#
#bks = breakdown(acts)
#bks = maketup(bks)
#
#print('='*80)
#print()
#for bd in bks:
#  print(f'{bd}\n')

def runtests():
    fail = False

    acts = {1}
    acts_exp = {frozenset({frozenset({1})})}
    if(breakdown(acts) != acts_exp):
        fail = True
        print(f'TEST CASE FAILED: {acts}')
    else:
        print('.')

    acts = {1,2}
    acts_exp = set()
    acts_exp.add(frozenset({ frozenset({1}), frozenset({2}) }))
    acts_exp.add(frozenset({frozenset({1,2})}))
    if(breakdown(acts) != acts_exp):
        fail = True
        print(f'TEST CASE FAILED: {acts}')
    else:
        print('.')

    acts = {1,2,3}
    acts_exp = set()
    acts_exp.add(frozenset({frozenset({1}),frozenset({2}),frozenset({3})}))
    acts_exp.add(frozenset({frozenset({1,2}),frozenset({3})}))
    acts_exp.add(frozenset({frozenset({1,3}),frozenset({2})}))
    acts_exp.add(frozenset({frozenset({2,3}),frozenset({1})}))
    acts_exp.add(frozenset({frozenset({1,2,3})}))
    if(breakdown(acts) != acts_exp):
        fail = True
        print(f'TEST CASE FAILED: {acts}')
    else:
        print('.')

    acts = {1,2,3,4}
    acts_exp = set()
    acts_exp.add(frozenset({ frozenset({1}), frozenset({2}), frozenset({3}), frozenset({4}) }))
    acts_exp.add(frozenset({ frozenset({1,2}),frozenset({3}),frozenset({4}) }))
    acts_exp.add(frozenset({ frozenset({1,2}),frozenset({3,4}) }))
    acts_exp.add(frozenset({frozenset({1,3}),frozenset({2}),frozenset({4})}))
    acts_exp.add(frozenset({frozenset({1,3}),frozenset({2,4})}))
    acts_exp.add(frozenset({frozenset({1,4}),frozenset({2}),frozenset({3})}))
    acts_exp.add(frozenset({frozenset({1,4}),frozenset({2,3})}))
    acts_exp.add(frozenset({frozenset({2,3}),frozenset({1}),frozenset({4})}))
    acts_exp.add(frozenset({frozenset({2,3}),frozenset({1,4})}))
    acts_exp.add(frozenset({frozenset({2,4}),frozenset({1}),frozenset({3})}))
    acts_exp.add(frozenset({frozenset({2,4}),frozenset({1,3})}))
    acts_exp.add(frozenset({frozenset({3,4}),frozenset({1}),frozenset({2})}))
    acts_exp.add(frozenset({frozenset({3,4}),frozenset({1,2})}))
    acts_exp.add(frozenset({frozenset({1,2,3}),frozenset({4})}))
    acts_exp.add(frozenset({frozenset({1,2,4}),frozenset({3})}))
    acts_exp.add(frozenset({frozenset({1,3,4}),frozenset({2})}))
    acts_exp.add(frozenset({frozenset({2,3,4}),frozenset({1})}))
    acts_exp.add(frozenset({frozenset({1,2,3,4})}))
    if(breakdown(acts) != acts_exp):
        fail = True
        print(f'TEST CASE FAILED: {acts}')
        for bd in breakdown(acts):
            if bd not in acts_exp:
                print(f'DISCREPANCY: {bd} not expected')
        for exp in acts_exp:
            if exp not in breakdown(acts):
                print(f'DISCREPANCY: {exp} expected but not seen')
    else:
        print('.')

    print('Starting large data set...', end='', flush=True)
    acts = {1,2,3,4,5,6,7,8,9,10}
    breakdown(acts)
    print('Done.')

    if(not fail):
        print('All tests passed.')

runtests()
