import random as R
import datetime
import time
# Does breakdowns as a packing problem, rather than the previous approach

MAX_TON = 440

def breakdown(x):
    x = sorted(x, key=lambda x: x['tonneage'], reverse=True) # sorted largest to smallest by tonneage

    groups = []

    i = 0
    while i < len(x):
        group_total = 0

        j = i
        while j < len(x) and group_total + x[j]['tonneage'] <= MAX_TON:
            group_total += x[j]['tonneage']
            j += 1

        groups.append(x[i:j])

        i = j # last loop of the while loop will result in j being i+1

    return groups


    

def run_abc_tests():
    a = {}
    b = {}
    c = {}

    a['es'] = 0
    b['es'] = 0
    c['es'] = 0

    a['duration'] = 1
    b['duration'] = 2
    c['duration'] = 3

    a['delay'] = 2
    b['delay'] = 2
    c['delay'] = 2

    a['tonneage'] = 7
    b['tonneage'] = 4
    c['tonneage'] = 3

    for group in breakdown([a,b,c]):
        print(group)

def run_random_tests(num, printgroups=True, printsums=False):
    items = []

    for i in range(num):
        items.append({'id': i, 'tonneage':R.randint(1,MAX_TON)})

    print('Input list:')
    for i in sorted(items, key=lambda x: x['tonneage'], reverse=True):
        print(i)

    print('Breakdown groupings:')
    groups = breakdown(items)


    for group in sorted(groups, key=lambda y: sum(map(lambda x: x['tonneage'], y)) ):
        if(printgroups):
            print(f'G: {group}')
        if(printsums):
            ss = sum(map(lambda x: x['tonneage'],group))
            print(f'Sum: {ss}')

#def calc_complexity():
#    data = []
#    for n in range(3, 10000):
#        items = []
#
#        for i in range(n):
#            items.append({'id': i, 'tonneage':R.randint(1,MAX_TON)})
#        starttime = datetime.datetime.today()
#        breakdown(items)
#        deltatime = datetime.datetime.today() - starttime
#        data.append((n, str(deltatime)))

run_random_tests(15, True, False)

#calc_complexity()
