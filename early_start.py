#! /usr/bin/env python3
import csv as csv
import networkx as nx
import itertools
import sys
import os
from typing import List, Mapping, Dict, Any, Tuple

#######################################
#Variables to adjust before the run
########################################
# Constants
#MAX_RESOURCE = lambda tp: 4400 if tp <= 365 else 4800 # in tons per time period #for the first year, (first 365 activities), make it 4400, then upgrade to 4700 past that
HEAT_CONSTRAINTS = [
931.202153162,
916.720240799999,
877.654385698,
838.9308648,
819.22747433,
800.036176799999,
780.241944377,
760.844739748,
746.154151618999,
721.805730488,
697.457309357,
672.392269827999,
648.712107566999,
624.412051437,
604.217432857
]

# Associates each resource with its column index in the .blocks file
# (not including the activity column, due to the way it is parsed)
# think of the activity column as a sort of column -1
RES_IDX_MAP = {
    'ORE_TONS':2,
    'PROD_DRILL_M':3,
    'PROD_DRILL_UNIT':4,
    'SLOT_RAISE_UNIT':5,
    'DROP_RAISE_UNIT':6,
    'ORE_CONTROL_UNIT':7,
    'ITH_UNIT':8,
    'DEVELOPMENT_FEET':9,
    'HEAT':10
}

# Globals
USE_RESOURCES = True
USE_HEAT = False
########################################

#class activity:
#    def __init__(self, uid, duration=None, resources=None):
#        self.uid = uid
#        self.duration = duration
#        self.resources = resources
#        self.pred_delays = {}
#
#    def add_pred(self, pred_uid, delay):
#        self.pred_delays[pred_uid] = delay
#
#    def __str__(self):
#        tmp_str = ''
#        tmp_str += (f'Act UID: {self.uid}\n')
#        tmp_str += (f'Duration: {self.duration}\n')
#        tmp_str += (f'Resources: {self.resources}\n')
#        tmp_str += (f'Preds: {self.pred_delays}\n')
#        return tmp_str

def parse_csv(csv_path):
    #Read in the file and parse appropriately
    with open(csv_path, mode = 'r') as csv_file:
        # return the list form of the csv data
        return list(csv.reader(csv_file, delimiter = ',', lineterminator = '\n'))

def pretty_print(acts):
    with open("check.txt", 'w') as outfile:
        for activity in acts:
            outfile.write(str(acts[activity]))
            outfile.write('='*80)
            outfile.write('\n')
            outfile.write('='*80)
            outfile.write('\n')

# reads files of form:
# [activity] [token 1] [token 2] ... [token n]
# [activity] ...
def parse_space_separated_data(file_path) -> Dict[int, List[Any]]:
    # Read the delays file
    with open(file_path, mode='r') as infile:

        # Read in the whole file
        dat: str = infile.read()

        # Split it into lines, each one representing a node and its information
        dat_lines: List[str] = dat.split('\n')

        betterdat: List[List[str]] = []

        # Split each row into its tokens
        # Also, remove empty lines
        for row in dat_lines:
            if(len(row) > 0):
                betterdat.append(row.split())

        # Convert into dict where key:value is activity_uid:[tokens]
        dat_dict = {}
        for token_row in betterdat:
            # if the row has extra tokens, put them in the dict
            # else associate the dict with an empty array
            try:
                activity_uid = int(token_row[0])
            except ValueError:
                continue # header row, skip it
            if len(token_row) > 2:
                dat_dict[activity_uid] = token_row[1:]
            else:
                dat_dict[activity_uid] = []

        return dat_dict

# Special case of the space separated data format
# reads files of form:
# [activity] [number of following tokens] [token 1] [token 2] ... [token n]
# [activity] ...
def parse_activity_magnitude_data(file_path):
    # return the space separated parsing, but with the column indicating the number
    # of tokens removed

    unfiltered_data = parse_space_separated_data(file_path)

    filtered_data = {}

    for act_uid in unfiltered_data:
        filtered_data[act_uid] = unfiltered_data[act_uid][1:]

    return filtered_data

# TODO read max capacity from the RCP file when doing this
# and make sure to set max resource capacity accordingly
# for the rest of the run
def parse_rcp_file(file_path):
    with open(file_path) as infile:
        lines = infile.read().split('\n')

        num_resources = int((lines[0].split())[1])

        resource_data = {}
        succ_data = {}

        # Lines are not labeled with activity UIDs so gotta make my own
        # Skip the first two lines since they don't have duration/resource/succ information on them
        for idx, line in enumerate(lines[2:]):
            tokens = line.split()
            if not(len(tokens) > 0):
                continue
            act_uid = int(idx+1) # 1-based activity UIDs to match up with successor values in the file
            act_duration = float(tokens[0])
            act_resource_data = [float(x) for x in tokens[1:1+num_resources]]
            act_num_succ = int(tokens[1+num_resources])
            act_succ = []
            if(act_num_succ > 0):
                act_succ = [int(succ_id) for succ_id in tokens[1+num_resources+1:]]

            # now form each one of the dict entries for this activity

            # other code expects resources to be like [garbage] [duration] [ore_tons] [other resources...]
            resource_data[act_uid] = [0, act_duration] + act_resource_data

            succ_data[act_uid] = act_succ

        return (resource_data, succ_data)

def parse_heat_constraints(file_path):
    pass

def parse_level_data(file_path):
    level_data: Dict[int, Tuple[int, float]] = {}
    with open(file_path) as infile:
        lines = infile.readlines()
        for idx,line in enumerate(lines):
            tokens = line.split()
            act_uid = int(idx)
            level = 0
            heat = 0
            for jdx,tok in enumerate(tokens):
                heat_val = float(tok)
                if(heat_val > 0):
                    heat = heat_val
                    level = jdx
            level_data[act_uid] = (level,heat)
    return level_data

# Calculate the early starts
def calc_early_starts(G, topo_sort, resource_constraints, invalid_list):
    for node in topo_sort:
        if(node in invalid_list):
            G.nodes[node]['early_start'] = 999999 # arbitrarily large number
        else:
            G.nodes[node]['early_start'] = resource_early_start(G, node, resource_constraints, invalid_list)

            # sanity check
            if G.nodes[node]['early_start'] > 100000:
                print(f'WARNING! early start for node {node} is obscenely high ({G.nodes[node]["early_start"]}')

# Given an activity and a list of its predecessors, returns the early start for the activity.
def resource_early_start(G, activity, resource_constraints, invalid_list):
    global USE_RESOURCES

    #preds = activity.pred_delays
    preds = list(G.predecessors(activity))

    # Ignore predecessors that are in the invalid list
    preds = list(filter(lambda x: x not in invalid_list, preds))
    #print(f'{activity} | preds: {preds}')

    # An activity with no predecessors can start immediately
    if(len(preds) == 0):
        #print(f'{activity} | no predecessors, returning 0')
        return 0

    if(USE_RESOURCES):
        # list of predecessors split into groupings having the same early-start values
        pred_splits = group_by_es(G, preds)

        # Holds the "sum" (ES + duration + delay) for each split
        pred_splits_sums = []

        #DEBUG TODO
        helpful = {}
        
        # For each set of predecessors that all have the same ES
        for split in pred_splits:
            # find split early start and delay
            split_early_start = G.nodes[split[0]]['early_start']

            best_breakdown, split_duration, split_delay = breakdown(G, split, activity, resource_constraints, HEAT_CONSTRAINTS if USE_HEAT else None)

            #split_duration = best_breakdown[1]
            #split_delay = best_breakdown[2]

            # Represents what the ES for this activity would be if this split is chosen
            # in the optimum parallelization case given the resource constraints
            split_sum = split_early_start + split_duration + split_delay
            helpful[split_sum] = f'Node: {activity} Split ES: {split_early_start} Split dur: {split_duration} split delay: {split_delay} Result: {split_sum}'
            #split_sum = split_early_start + split_delay # delay incorporates duration from predecessor activity

            pred_splits_sums.append(split_sum)

        # Whatever split sum is the largest (i.e. finishes the latest) is the limiting factor preventing this
        # activity from starting, and thus becomes the ES for this activity
        #print(helpful[max(pred_splits_sums)])
        return max(pred_splits_sums)
    else:
        # Holds the "sum" (ES + duration + delay) for each predecessor
        pred_sums = []
        for pred in preds:
            pred_es = G.nodes[pred]['early_start']
            pred_duration = G.nodes[pred]['duration']
            pred_delay = G[pred][activity]['delay']
            pred_sums.append(pred_es + pred_duration + pred_delay)

        return max(pred_sums)


# Retuns a tuple (breakdown, breakdown_duration, breakdown_delay)
# Breakdown:
#   A list of lists of activities ("groups")
#   Each group is the most activities that can be parallelized while still
#   adhering to the resource contstraints
#   In physical terms, activities within the group can be run in parallel with each other 
#   but the groups themselves must be run sequentially
#   A grouping's grouping_delay is equal to the largest delay from that group
# Breakdown_delay:
#   The breakdown_delay is the sum of the grouping_delays
def breakdown(G, split, target_activity, resource_constraints, max_heat=None):
    global USE_HEAT

    # Sanity check
    if( (max_heat is not None) and (not USE_HEAT) ):
        print('[WARN] USE_HEAT is false but max_heat was passed, is this intended?')

    # Error check
    if( USE_HEAT and (max_heat is None) ):
        raise ValueError('breakdown must be passed a max_heat param if USE_HEAT set')

    # Ore tons check, make sure all resources are within limits
    for activity in split:
        for idx,resource in enumerate(G.nodes[activity]['resources']):
            if resource > resource_constraints[idx]['max']:
                raise ValueError(f'Resource #{idx} of activity {activity} ({resource}) exceeds maximum ({resource_constraints[idx]["max"]})')
                #raise ValueError(f'Tonneage of activity {activity} ({G.nodes[activity]["resources"]["ORE_TONS"]}) exceeds maximum')

    ## If there is more than one resource tracked
    #if(len(G.nodes[split[0]]['resources']) > 1):
    #    # pre-sort splits by these resources so that they serve as a tiebreaker
    #    for resource in G.nodes[split[0]]['resources']:
    #        if (resource != 'ORE_TONS'):
    #            split = sorted(split, key=lambda x: G.nodes[x]['resources'][resource], reverse=True)

    #sorted_split = sorted(split, key=lambda x: G.nodes[x]['resources']['ORE_TONS'], reverse=True) # sorted largest to smallest by tonneage

    # Sort in descending order by each of the resources tracked
    # starting with the last resource in the list and then finishing
    # by sorting according to the first resource in the list
    # Since the sort is stable, this means that the first resource in the list
    # will end up having the most sway over the ordering of the items
    sorted_split = None
    for resource_idx in reversed(range(len(G.nodes[split[0]]['resources']))):
        sorted_split = sorted(split, key=lambda x: G.nodes[x]['resources'][resource_idx], reverse=True)

    groups = []
    breakdown_duration = None
    breakdown_delay = None

    # Find Groups
    i = 0
    while i < len(sorted_split): # move left pointer through the activities
        group_resource_totals = [0 for x in resource_constraints]
        group_heat_total = {}

        if(USE_HEAT):
            for level in range(len(max_heat)):
                group_heat_total[level] = 0

        # collect activities with the right pointer until max resources reached
        j = i

        while j < len(sorted_split):
            node_j = G.nodes[sorted_split[j]]

            resources_acceptable = True
            for idx,resource in enumerate(node_j['resources']):
                resources_acceptable = resources_acceptable and (group_resource_totals[idx] + resource <= resource_constraints[idx]['max'])

            heat_acceptable = True
            if(USE_HEAT):
                level = node_j['level']
                heat_acceptable = group_heat_total[level] + node_j['heat'] <= max_heat[level]
                #print(f'Unacceptable heat at node {sorted_split[j]}, nothing more at this level for the rest of the sprint')

            acceptable = False
            if(USE_HEAT):
                acceptable = (resources_acceptable and heat_acceptable)
            else:
                acceptable = resources_acceptable

            if acceptable:
                for idx,resource in enumerate(node_j['resources']):
                    group_resource_totals[idx] += resource
                if(USE_HEAT):
                    group_heat_total[node_j['level']] += node_j['heat']
                j += 1
            else:
                break

        # take all the activities collected and make them into a group
        groups.append(sorted_split[i:j])

        # start after the end of that group and make another group
        i = j # last loop of the while loop will result in j being i+1

    # Calculate breakdown_duration
    # Sum of the grouping durations,
    # where grouping duration for each group is the max duration of all the members of that group
    # Intuitively, each group represents all the acts that can be done at the same time
    # Each group is not done until its longest-duration activity is done
    # And then to complete the breakdown, all groups must be done sequentially
    # So that means that the total duration for the breakdown is the sum of all the grouping durations
    #print(f'{target_activity} | Groupings: {groups}')
    grouping_durations = [max([G.nodes[pred]['duration'] for pred in split]) for grp in groups]
    #print(f'{target_activity} | Grouping durations: {grouping_durations}')
    breakdown_duration = sum(grouping_durations)

    # Find grouping delay from each grouping
    # This will be whatever delay goes with the longest duration activity from that group
    # Since that will be the delay that happens "last" and will thus take effect if this grouping
    # is chosen to be last in the order
    grouping_delays = []
    for grp in groups:
        longest_pred_in_group = grp[0]
        for act in grp:
            if G.nodes[act]['duration'] > G.nodes[longest_pred_in_group]['duration']:
                longest_pred_in_group = act
        grouping_delays.append( G[longest_pred_in_group][target_activity]['delay'] )

    # The min of the grouping_delays is what will be the breakdown delay
    # Since we can do the groups in the breakdown in any order (assumption),
    # we'll assume that we always do the one which allows us to have minimum delay last
    breakdown_delay = min(grouping_delays)

    return (groups, breakdown_duration, breakdown_delay)
    


def group_by_es(G, activities):
    keyfunc = lambda x: G.nodes[x]['early_start']
    rslt = [list(g) for k,g in itertools.groupby(sorted(activities, key=keyfunc), keyfunc)]
    return rslt


#def invalid_activities(G, topo_sort):
#    invalid_list = []
#
#    if(USE_RESOURCES):
#        for act in topo_sort:
#            if(G.nodes[act]['resources']['ORE_TONS'] > resource_constraints[0] ):
#                invalid_list.append(act)
#
#    return invalid_list

def res_name_to_blocks_index(res_name):
    return RES_IDX_MAP[res_name]

def print_usage():
    print('Usage:\n  early_start (<RCP file path> | <blocks file path> <pred file path> <delay file path>) (--no-resources | -r <KEY RESOURCE> [-r ...]) (-H -l=/path/to/activity/heat/level/file)')
    print('Add --no-resources to disable resource-based calculation.')
    print('Valid Key Resources:')
    for res in RES_IDX_MAP:
        print(f'  {res}')
    sys.exit(1)

def main():
    global USE_RESOURCES
    global USE_HEAT

    ## COMMAND LINE ARGS

    # Print usage information if malformed
    #if len(sys.argv) < 5 or (sys.argv[4] != '-r' and sys.argv[4] != '--no-resources'):
    #    print_usage()
    #    return
    resources_to_use = [] # list of resource columns from .blocks to use
                          # valid names:
                          # ORE_TONS
                          # PROD_DRILL_M
                          # PROD_DRILL_UNIT
                          # SLOT_RAISE_UNIT
                          # DROP_RAISE_UNIT
                          # ORE_CONTROL_UNIT
                          # ITH_UNIT
                          # DEVELOPMENT_FEET
                          # HEAT
    #infile_directory = None
    resource_file = None
    pred_file = None
    delay_file = None
    prob_file = None

    # heat files
    #heat_constraints_file = None
    level_file = None

    use_rcp_file = False
    rcp_file = None

    duration_column = 0

    argmode = 'default' # arg processing argmode
    for idx, arg in enumerate(sys.argv):
        if(idx == 1):
            if(arg.split('.')[-1].upper() == 'RCP'):
                use_rcp_file = True
                rcp_file = arg
            else:
                resource_file = arg
        elif(idx == 2 and not use_rcp_file):
                pred_file = arg
        elif(idx == 3 and not use_rcp_file):
            delay_file = arg
        elif(argmode == 'default'):
            if(arg == '-r'):
                if(USE_RESOURCES):
                    argmode = 'resource'
                else:
                    print('Cannot specify resources if --no-resources set!')
                    print_usage()
                    return
            if(arg == '--no-resources'):
                if(len(resources_to_use) == 0):
                    USE_RESOURCES = False
                else:
                    print('Cannot specify resources if --no-resources set!')
                    return
            if(arg == '-H'):
                USE_HEAT = True
            #if('-h=' in arg):
            #    heat_constraints_file = arg.split('=')[1]
            if('-l=' in arg):
                level_file = arg.split('=')[1]
            if('--dur-col=' in arg):
                duration_column = int(arg.split('=')[1])
            if('--prob-file=' in arg):
                prob_file = arg.split('=')[1]
            #elif(arg == '-d'):
            #    argmode = 'directory'
        elif(argmode == 'resource'):
            resources_to_use.append(arg)
            argmode = 'default'
        #elif(argmode == 'directory'):
        #    infile_directory = arg
        #    argmode = 'default'

    #if(infile_directory is None):
    #    print_usage()
    #    return
    if(rcp_file is None and (resource_file is None or pred_file is None or delay_file is None)):
        print_usage()

    #resource_file = 'infiles/Small-Limited/Subika_LT0.blocks' # provides activity, profit, duration, resources
    #pred_file = 'infiles/Small-Limited/Subika_LT0.fpp' # provides activity and predecessors
    #delay_file = 'infiles/Small-Limited/Subika_LT0.delay' # provides activity and delays from each predecessor

    if(use_rcp_file):
        case_name = rcp_file.split("/")[2].split(".")[0]
        filename_stem = f'outfiles/{case_name}-es'
    else:
        case_name = resource_file.split("/")[2].split(".")[0]
        if(case_name == 'Subika_LT0'):
            case_name = resource_file.split("/")[1]
        filename_stem = f'outfiles/{case_name}-es'

    if(USE_HEAT):
        filename_stem += '-heat'

    output_file = f'{filename_stem}.csv'

    counter = 1
    while(os.path.isfile(output_file)):
        print(f'Trying {output_file}')
        output_file = f'{filename_stem}-{counter}.csv'
        counter += 1
    print(f'Outputting to {output_file}')

    resource_data = None
    pred_data = None
    delay_data = None
    resource_constraints = []

    # Heat things
    level_data = None
    heat_constraints_data = None

    succ_data = None

    #Variable Creation
    #acts = {}
    G = nx.DiGraph() #Empty directed networkx graph to put all of the nodes and arcs in
    topo_sort = []

    # Parse out constraints from .prob file
    with open(prob_file) as infile:
        for line in infile.readlines():
            tokens = line.split()
            if('CONSTRAINT:' in tokens[0]):
                rsrc_id = int(tokens[1])
                rsrc_col = int(tokens[2])-1 # convert to numbering scheme used elsewhere
                rsrc_max = float(tokens[6])
                resource_constraints.append({'col':rsrc_col, 'max':rsrc_max})
            if len(resource_constraints) == 8:
                break


    if(use_rcp_file):
        resource_data, succ_data = parse_rcp_file(rcp_file)
    else:
        resource_data = parse_space_separated_data(resource_file)

        pred_data = parse_activity_magnitude_data(pred_file)

        delay_data = parse_activity_magnitude_data(delay_file)

        # Convert delays to floats
        for act_uid in delay_data:
            delay_data[act_uid] = [float(delay) for delay in delay_data[act_uid]]
            #print(f'Delay of {act_uid} is read to be {delay_data[act_uid]}')

    if(USE_HEAT):
        level_data = parse_level_data(level_file)
        heat_constraints_data = HEAT_CONSTRAINTS

    #Convert column entries to floats
    for act_uid in resource_data:
        resource_data[act_uid] = [float(i) for i in resource_data[act_uid]]

    #Loop through activities once to create the nodes
    for act_uid in resource_data:
        #Add nodes and give attributes for duration, early start time, and resource information

        # Construct a dict mapping resource names to their particular value for this activity
        # Ex. other_resources = {'ORE_TONS': resource_data[act_uid][2]}
        other_resources = []
        # Old way of handling resources
        #for res in resources_to_use:
        #    res_name = res
        #    res_idx = res_name_to_blocks_index(res_name)
        #    other_resources[res_name] = resource_data[act_uid][res_idx]
        for resource in resource_constraints:
            other_resources.append(resource_data[act_uid][resource['col']])


        #G.add_node(act.uid, duration = act.duration, resources = act.resources, early_start = 0)
        activity_duration = resource_data[act_uid][duration_column]

        # Raise an error if the duration is not an integer
        if activity_duration - int(activity_duration) != 0:
            raise ValueError(f'Activity duration must be an integer (actually got {activity_duration}), do you have the right column?')
            sys.exit(1)

        G.add_node(act_uid, early_start = 0, duration=activity_duration, resources=other_resources)
        #print(f'Duration of {act_uid} is {resource_data[act_uid][0]}')

        if(USE_HEAT):
            G.nodes[act_uid]['level'], G.nodes[act_uid]['heat'] = level_data[act_uid]
            


    # Create activity objects containing this information
    #for act_uid in resource_data:

    #    # Construct a dict mapping resource names to their particular value for this activity
    #    # Ex. other_resources = {'ORE_TONS': resource_data[act_uid][2]}
    #    other_resources = {}
    #    for res in resources_to_use:
    #        res_name = res
    #        res_idx = res_name_to_blocks_index(res_name)
    #        other_resources[res_name] = resource_data[act_uid][res_idx]

    #    new_act = activity(act_uid, duration=resource_data[act_uid][1], resources=other_resources)
    #    acts[new_act.uid] = new_act


    #Loop through and create edges
    if(use_rcp_file):
        # TODO not sure if this works (specifically the way I'm fetching act uid and passing into succ_data as index), test with RCP file
        for act_uid in resource_data:
            # Add an edge from the node to its successors
            for succ_uid in succ_data[act_uid]:
                G.add_edge(act_uid, succ_uid, delay=0)
    else:
        for act_uid in pred_data:
            for pred_idx,pred_uid in enumerate(pred_data[act_uid]):
                pred_uid = int(pred_uid)
                # Delay information in the file is actually delay + duration
                # so subtract out the duration of the predecessor to get pure delay
                delay = delay_data[act_uid][pred_idx] - G.nodes[pred_uid]['duration']
                #print(f'Delay from {pred_uid} to {act_uid} is {delay}')
                #acts[act_uid].add_pred(pred_uid, delay)
                # Add an edge from each predecessor to this node
                G.add_edge(pred_uid, act_uid, delay=delay)

            
    # Use a topological sort as the order in which we calculate early starts
    topo_sort = list(nx.topological_sort(G))

    # Get a list of all activities which exceed the max per-time-period resource constraint
    # so that they can be ignored later
    #ignore_list = invalid_activities(G, topo_sort)
    ignore_list = []

    # Perform the early start algorithm on the sorted list
    # Early starts are placed in G
    calc_early_starts(G, topo_sort, resource_constraints, ignore_list)


    #Output the Early start times
    with open(output_file, mode = 'w') as writer:
        #Write out early starts in the same activity order as provided
        for act_uid in G.nodes:
            try:
                writer.write(str(act_uid)+','+ str(int(G.nodes[act_uid]['early_start']) + 1)+'\n')
            except ValueError:
                writer.write(str(act_uid)+','+ str(G.nodes[act_uid]['early_start'])+'\n')

if __name__=='__main__':
    main()
